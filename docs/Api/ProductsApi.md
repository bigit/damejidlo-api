# Swagger\Client\ProductsApi

All URIs are relative to *https://catalog-backlog-stg.deliveryhero.io/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**patchVendorProductPriceAndActive**](ProductsApi.md#patchvendorproductpriceandactive) | **PATCH** /v1/vendors/{vendorId}/skus/{sku} | PATCH operation of a product by price and/or active

# **patchVendorProductPriceAndActive**
> patchVendorProductPriceAndActive($body, $vendor_id, $sku)

PATCH operation of a product by price and/or active

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Swagger\Client\Model\PatchVendorProductPriceAndActiveRequest(); // \Swagger\Client\Model\PatchVendorProductPriceAndActiveRequest | 
$vendor_id = 56; // int | 
$sku = "sku_example"; // string | 

try {
    $apiInstance->patchVendorProductPriceAndActive($body, $vendor_id, $sku);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->patchVendorProductPriceAndActive: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\PatchVendorProductPriceAndActiveRequest**](../Model/PatchVendorProductPriceAndActiveRequest.md)|  |
 **vendor_id** | **int**|  |
 **sku** | **string**|  |

### Return type

void (empty response body)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)


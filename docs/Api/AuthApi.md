# Swagger\Client\AuthApi

All URIs are relative to *https://catalog-backlog-stg.deliveryhero.io/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**auth**](AuthApi.md#auth) | **POST** /auth | auth
[**refreshToken**](AuthApi.md#refreshtoken) | **POST** /auth/refresh | Refresh Token

# **auth**
> \Swagger\Client\Model\TokenPayload auth($body)

auth

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\AuthApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Swagger\Client\Model\AuthRequest(); // \Swagger\Client\Model\AuthRequest | This call expects email and password that exist in catalog's database. It returns a token that should be passed to other calls in the Authorization header as a Bearer token.

try {
    $result = $apiInstance->auth($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AuthApi->auth: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\AuthRequest**](../Model/AuthRequest.md)| This call expects email and password that exist in catalog&#x27;s database. It returns a token that should be passed to other calls in the Authorization header as a Bearer token. |

### Return type

[**\Swagger\Client\Model\TokenPayload**](../Model/TokenPayload.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **refreshToken**
> \Swagger\Client\Model\TokenPayload refreshToken($body)

Refresh Token

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\AuthApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Swagger\Client\Model\TokenPayload(); // \Swagger\Client\Model\TokenPayload | 

try {
    $result = $apiInstance->refreshToken($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AuthApi->refreshToken: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\TokenPayload**](../Model/TokenPayload.md)|  |

### Return type

[**\Swagger\Client\Model\TokenPayload**](../Model/TokenPayload.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

